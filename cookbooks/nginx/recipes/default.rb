execute 'apt-get-update' do
  command 'apt-get -y update'
end

apt_package 'nginx'

template '/etc/nginx/sites-available/default' do
  source 'nginx-frontend.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service 'nginx' do
  action :restart
end
