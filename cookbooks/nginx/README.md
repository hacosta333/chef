I did this cookbook without using the official one because the official has a lot of code that is unnecesary for this exercise.

This cookbook is very simple:

* For Frontend it installs nginx, copies a template to do a proxypass to the backend and restarts nginx.
* For Backend it only installs nginx because I´m using the default index to serve something.

Thanks so much!

