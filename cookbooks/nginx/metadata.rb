name             'nginx'
maintainer       'Henry Acosta'
maintainer_email 'hacosta333@gmail.com'
license          'Public domain'
description      'Installs nginx server'
version          '1.0.0'
issues_url       'www.issuesurl.com'
source_url       'www.sourceurl.com'
chef_version     '>= 14.11.21'
license          'GPL-3.0'
supports         'ubuntu'
